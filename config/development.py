from decouple import config

SECRET_KEY = config('SECRET_KEY')

SQLALCHEMY_DATABASE_URI = 'sqlite:///sqlite3.db'

SQLALCHEMY_TRACK_MODIFICATIONS = False