from decouple import config


SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test_app.db'

SQLALCHEMY_TRACK_MODIFICATIONS = False

DEBUG = True 

TESTING = True 

WTF_CSRF_ENABLED = False

SECRET_KEY = config('SECRET_KEY')