import sys 
import pyinputplus as pyip
from interviewapp.users.models import User
from sqlalchemy.exc import SQLAlchemyError


def check_password(passw1, passw2):
    return passw1 == passw2

def get_user_input():
    username = pyip.inputStr(prompt="username: ")
    email = pyip.inputEmail(prompt="email: ")
    password1 = pyip.inputPassword(prompt="password: ")
    password2 = pyip.inputPassword(prompt="confirm password: ", )
    return username, email, password1, password2

try:
    command = sys.argv[1]
    if command == 'createsuperuser':
        username, email, password1, password2 = get_user_input()
        if check_password(password1, password2):
            try:
               User.create_user(username=username, email=email, password=password1)
            except SQLAlchemyError:
                print("could not add super user")
        else:
            print("password don't match ")

    if command == 'createuser':
        while True:
           username, email, password1, password2 = get_user_input()

           if check_password(password1, password2):
               User.create_user(username=username, email=email, password=password1)
               break
           else:
               print("password don't match \n")
            
    else:
        print("command {} not found".format(command))
except IndexError:
    print("missing command")