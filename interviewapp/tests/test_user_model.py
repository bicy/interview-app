from interviewapp.users import models

def test_create_user_instance(session):
    """Create and save a user instance"""

    email = 'test@example.com'
    username = 'test_user'
    password = 'foobarbaz'

    user = models.User(email=email, username=username, password=password)
    session.add(user)
    session.commit()

    assert user.id is not None
    