
from flask import session, get_flashed_messages
from interviewapp.users.models import User
from interviewapp import flask_bcrypt

def test_get_user_login_page(client):
    """Ensure login page is available."""
    # get the page using url
    response = client.get('/users/login/')
    # assert response status code to be 200 
    assert response.status_code == 200
    # get response data as text or string
    data = response.get_data(as_text=True)
    assert 'Sign in' in data

def test_add_new_user(client):
    """ Successfully adding new user."""

    data = {'username':'test_username', 'email':'test@example.com'}

    response = client.post('/users/add/', data=data)

    # On successfull creation we redirect
    assert response.status_code == 302

    assert 'id' in session

    assert get_flashed_messages() == []

