import random

def _code_generator():
    digit = ""
    for x in range(3):
        digit = digit + str(random.randint(1,9))
    return digit

def get_unique_code():
    code = "{0}-{1}-{2}".format(_code_generator(),_code_generator(),_code_generator())
    return code

def get_username_list(users):
    usernames = []
    for user in users:
        usernames.append(user.username)
    return usernames
