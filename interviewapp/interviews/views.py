from flask import (
    Blueprint, 
    flash, redirect, 
    render_template, 
    url_for, request, json, jsonify)

from sqlalchemy import desc
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import exc
from flask_login import login_required, current_user
from interviewapp import db
from .models import Interview
from interviewapp.interviews.forms import AddInterviewForm
from interviewapp import script

from interviewapp.users.models import User
from interviewapp.topics.models import Topic
from interviewapp.questions.models import Question
from interviewapp.rates.models import Rate

from interviewapp import script


interviews = Blueprint('interviews', __name__, template_folder='templates/interviews')

@interviews.route("/", methods=["GET", "POST"])
@login_required
def home():
    page_title = "Home"
    form = AddInterviewForm()
    form.user.choices = User.query.filter_by(is_specialist=True).all()
    topics = Topic.query.all()
    interviews = Interview.query.order_by(desc(Interview.created_at))[:9]
    questions = Question.query.all()
    if request.method == 'POST':
        if form.validate_on_submit():
            current_user_id = current_user.id
            new_interview = form.save(current_user_id)
            questions = request.form.getlist('question')
            for id in questions:
                q = Question.query.filter_by(id=id).first()
                new_interview.questions.append(q)
            try:
                db.session.add(new_interview)
                db.session.commit()
                flash("Interview successfully added", "success")
                return redirect(url_for("interviews.listing"))
            except exc.SQLAlchemyError:
                flash("something went wrong wen creating interview", "success")
        else:
            flash("Invalid data submited",'danger')


    if current_user.is_specialist:
        return redirect(url_for('interviews.expert_interview_list'))
    return render_template(
        "base.html",
        page_title=page_title,
        form=form,
        interviews=interviews,
        topics=topics,
        questions=questions,
    )



@interviews.route("/<id>/detail/", methods=["POST", "GET"])
@login_required
def detail(id):
    # get interview object by id
    interview = Interview.query.filter_by(id=id).first()
    page_title = "{0} detail".format(interview.title)
    # send json data if header accept json
    if request.is_json:
        json_data = json.dumps(interview)
        return jsonify({"interview": json_data})

    # render html data
    return render_template(
        "interviews/interview_detail.html", 
        interview=interview,page_title=page_title)
    
@interviews.route("/listing/expert",methods=['POST','GET'])
def expert_interview_list():
    page_title = 'interviews'
    current_user_name = current_user.username
    interviews = Interview.query
    interview_list = []
    for interview in interviews:
        for username in script.get_username_list(interview.users):
            if current_user_name == username:
                interview_list.append(interview)

    print(interview_list)
    return render_template('interviews/interview_list.html', page_title=page_title, interviews=interview_list)


@interviews.route("/listing/", methods=['POST','GET'])
@login_required
def listing():

    if current_user.is_specialist:
        return redirect(url_for('interviews.expert_interview_list'))

    page_title = "Interviews"
    interviews = None
    if current_user.is_admin:
        interviews = Interview.query.order_by(desc(Interview.created_at))[:9]
    else:
    # get interviews and ordering by create date LIMIT 9
        interviews = Interview.query.filter_by(user_id=current_user.id).all()
    return render_template(
        'interviews/interview_list.html', 
        page_title=page_title, 
        interviews=interviews
        )


@interviews.route("/search/", methods=['POST', 'GET'])
@login_required
def search():
    # request send using post method
    if request.method == 'POST':
        interviews_data = Interview.query
        # query enter by user using search field
        query = request.form.get('search')
        # if query is empty
        if not query:
            # send flash message of not found 
            flash("warning: empty query was sent",'warning')
            # redirect user to interview list page
            return redirect(url_for('interviews.listing'))
        
        # if query is not empty
        page_title = "{0} interviews".format(query)
        # filter interviews using the query
        matching = interviews_data.filter(Interview.title.like("%" + query + "%")).all()
        # if query word not found in database
        if matching == None:
            # send flash message to interview list
            flash('{0} does not match any interview'.format(query),'info')
            return redirect(url_for('interviews.listing'))
    
    # if GET request is sent
    return render_template(
        'interviews/search_result.html', 
        page_title=page_title, 
        interviews=matching,
        query = query
    )

#delete route is used for deleting interview
@interviews.route("/<id>/delete/", methods=['POST','GET'])
@login_required
def delete(id):
    # get interview to be deleted using id
    interview = Interview.query.filter_by(id=id).first()
    # set the page title 
    page_title = "Delete | {0} ".format(interview.title)
    # if request method is POST
    if request.method == "POST":
        # get interview id send though POST method using hidden field 
        id = request.form.get("id")
        # get interview from database by id
        interview = Interview.query.filter_by(id=id).first()
        try:
            # delete interview from database
            db.session.delete(interview)
            # save changes done in database
            db.session.commit()
            # send flash message to interviews list page
            flash("{0} successfully deleted".format(interview.title), 'success')
            return redirect(url_for('interviews.listing'))
        except SQLAlchemyError as e:
            # notify user that interview could not be deleted
            flash("{0} could not be deleted".format(interview),'warning')
            # redirect user to interview list page
            return redirect(url_for('interviews.listing'))

    # if request method is GET
    return render_template(
        "interviews/interview_delete.html",
         page_title=page_title,
         interview=interview
    )
   

@interviews.route("/<id>/add/question", methods=["POST","GET"])
@login_required
def interview_add_question(id):
    id = id
    topics = Topic.query.all()
    interview = Interview.query.filter_by(id=id).first()
    page_title = "Question | {0}".format(interview.title)
    if request.method == 'POST':
        interview_id = request.form.get('interview_id')
        interview = Interview.query.filter_by(id=interview_id).first()
        id_list = request.form.getlist('question')
    
        for question_id in id_list:
            q = Question.query.filter_by(id=question_id).first()
            interview.questions.append(q)
        db.session.commit()
        
        flash("Question successfully added",'success')
        return redirect(url_for("interview_detail", id=id))

    return render_template(
        "interviews/add_question.html",
        id=id,
        page_title=page_title, 
        topics=topics
        )


@interviews.route("/<inter>/question/<quest>/rate",methods=["POST","GET"])
@login_required
def interview_rate_question(inter, quest):
    # get interview whose question to be rated
    interview_object = Interview.query.filter_by(id=inter).first()

    quest_id = quest
    inter_id = inter
    # get question to be rated
    question_object  = Question.query.filter_by(id=quest).first()

    if request.method == 'POST':
        # user sends an empty field 
        if not request.form.get("username"):
            flash("Evaluator username is required",'danger')
            return redirect(url_for("interview_rate_question",inter=inter, quest=quest))
        else:
            username = request.form.get('username')
            # get user username 
            usernames = script.get_username_list(interview_object.users)
            # if the username matches any evaluator name
            if username in usernames:
                flash("{0} username matches".format(username), 'success')
                return redirect(url_for('rate_question',quest=quest_id, inter=inter_id))
            
            # username not found 
            else:
                flash("Evaluator no found", 'danger')
                return redirect(url_for("interview_rate_question",inter=inter, quest=quest))
            
    return render_template(
        "rate_auth.html",
        question = question_object,
        interview = interview_object
    )
    

@interviews.route("/<id>/rate/",methods=["POST", "GET"])
@login_required
def rate_question(id):
    interview = Interview.query.filter_by(id=id).first()
    if request.method == 'POST':
        rate_value = request.form.get('rate')
        user_id = request.form.get("user_id")
        if not rate_value or user_id == 'None':
            flash("Rate and evaluator are required", 'danger')
        else:
            quest_id = request.form.get('question_id')
            rate = Rate(
                    user_id=user_id, 
                    question_id=quest_id, 
                    value=rate_value,
                    interview_id=interview.id
                )
            try:
                db.session.add(rate)
                db.session.commit()
                flash("successully rated",'success')
            except exc.SQLAlchemyError as e:
                # log the error
                flash("something went wrong when rating ",'danger')

    return redirect(url_for('interviews.detail',id=interview.id))
    



