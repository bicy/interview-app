from datetime import datetime
from interviewapp import db
from interviewapp.questions.models import Question
from interviewapp.users.models import User


question_association_table = db.Table(
    "question_association",
    db.metadata,
    db.Column("question_id", db.ForeignKey("question.id")),
    db.Column("interview_id", db.ForeignKey("interview.id")),
)

association_table = db.Table(
    "association",
    db.metadata,
    db.Column("interview_id", db.ForeignKey("interview.id")),
    db.Column("user_id", db.ForeignKey("user.id")),
)

class Interview(db.Model):
    __tablename__ = "interview"
    __table_args__ = {"extend_existing": True}

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30),nullable=False)
    candidate_name = db.Column(db.String(80))
    interview_id = db.Column(db.String(11), unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    users = db.relationship("User", secondary=association_table)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date = db.Column(db.DateTime)
    final_score = db.Column(db.Integer)
    questions = db.relationship("Question", secondary=question_association_table)
    rates = db.relationship("Rate", backref="rate", lazy=True)
  

    def __repr__(self):
        return "{0} {1}".format(self.title, self.created_at)