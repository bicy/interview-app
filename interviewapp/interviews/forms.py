from flask_wtf import FlaskForm 
from wtforms import (
     SelectField, 
     SelectMultipleField, 
     DateField, StringField,
     SubmitField, 
     SearchField)
from wtforms.validators import DataRequired, Length


from interviewapp.interviews.models import Interview
from interviewapp.questions.models import Question
from interviewapp.users.models import User
from interviewapp.topics.models import Topic

from interviewapp import script
from interviewapp import db 
from flask_login import current_user


class AddInterviewForm(FlaskForm):

    candidate_name = StringField(
        "Candidate name", validators=[Length(min=5, max=60), DataRequired()]
    )
    title = StringField("Title", validators=[Length(min=5, max=60), DataRequired()])
    user = SelectMultipleField("Experts", choices=[], validate_choice=False)
    date = DateField("Interview date")
    submit = SubmitField("Submit")

    def save(self, user_id):
        title = self.title.data
        name = self.candidate_name.data
        date = self.date.data
        users = self.user.data
        code = script.get_unique_code()
        interview = Interview(
            title=title,
            candidate_name=name,
            user_id=user_id,
            date=date,
            interview_id=code,
        )
        for username in users:
            user = User.query.filter_by(username=username).first()
            interview.users.append(user)

    
        return interview

class SearchForm(FlaskForm):
    search = SearchField("Search", validators=[DataRequired()])


