import pytest
import os 
from interviewapp import create_app, db  as database

DB_LOCATION = '/tmp/test_app.db'

@pytest.fixture(scope='session')
def app():
    app = create_app(config='config.test_settings')
    return app

@pytest.fixture(scope='session')
def db(app, request):
    """ Session-wide test database"""

    # check if test_app.db file exists
    if os.path.exists(DB_LOCATION):
        # let delete test_app.db for another test
        os.unlink(DB_LOCATION)

    database.app = app
    database.create_all()

    def teardown():
        database.drop_all()
        os.unlink(DB_LOCATION)

    request.addfinalizer(teardown)
    return database


@pytest.fixture(scope='function')
def session(db, request):
    session = db.create_scoped_session()
    db.session = session

    def teardown():
        session.remove()

    request.addfinalizer(teardown)
    return session