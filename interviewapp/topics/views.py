from flask import Blueprint, flash, redirect, url_for, render_template,request
from flask_login import login_required
from interviewapp.topics.models import Topic
from interviewapp.topics.forms import TopicAddForm

topics = Blueprint('topics', __name__)

# topic view
@topics.route("/listing")
@login_required
def listing():
    page_title = "Topics"
    topics = Topic.query.all()
    return render_template("topics/index.html", topics=topics, page_title=page_title)

@topics.route("/add", methods=['POST', 'GET'])
def add():
    form = TopicAddForm()
    topics = Topic.query.all()
    if request.method == 'POST':
        if form.validate_on_submit():
            form.save()
            flash("Topic added success fully",'success')
            topics = Topic.query.all()
            return redirect(url_for('topics.listing'))
        else:
            flash("Invalid data", 'danger')
            return redirect(url_for('topics.add'))
    return render_template("topics/topic_add.html", topics=topics, form=form)