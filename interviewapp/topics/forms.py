from wtforms.validators import Length, DataRequired
from wtforms import StringField, SubmitField
from flask_wtf import FlaskForm
from interviewapp import db

from interviewapp.topics.models import Topic

class TopicAddForm(FlaskForm):
    title = StringField("Title", validators=[DataRequired(),Length(max=50, min=3)])
    submit = SubmitField("Submit")

    def save(self):
        topic = Topic(content=self.title.data)
        db.session.add(topic)
        db.session.commit()