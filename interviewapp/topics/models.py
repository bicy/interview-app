from interviewapp import db 

topic_question = db.Table(
    "topic_question",
    db.metadata,
    db.Column("topic_id", db.ForeignKey("topic.id")),
    db.Column("question_id", db.ForeignKey("question.id")),
)


class Topic(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(80), unique=True)
    questions = db.relationship("Question", secondary=topic_question)
       

    def __repr__(self):
        return "{0}".format(self.content)