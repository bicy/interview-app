$(document).ready(function () {

    let question_select = document.getElementById("question");

    $("#topic").change(function () {
        topic_id = $(this).val();
        fetch('/questions/' + topic_id).then(function (response) {
            response.json().then(function (data) {
                let optionHTML = '';
                for (let q of data.questions) {
                    optionHTML += '<option value="' + q.id + '">' + q.content + '</option>';
                }

                question_select.innerHTML = optionHTML;
            });
        })
    });


});