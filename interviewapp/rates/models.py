from interviewapp import db 

class Rate(db.Model):
    __tablename__ = "rate"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey("question.id"), nullable=False)
    value = db.Column(db.Integer)
    interview_id = db.Column(db.Integer, db.ForeignKey('interview.id'), nullable=False)

