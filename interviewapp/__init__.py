from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from sqlalchemy.ext.hybrid import hybrid_property
from flask_login import LoginManager
from decouple import config 



# Initialize db extension, but without configuring
# it with an application instance.
db = SQLAlchemy()
migrate = Migrate()
flask_bcrypt = Bcrypt()
login_manager = LoginManager()


# factory application
def create_app(config=None):
    app = Flask(__name__)

    if config is not None:
        app.config.from_object(config)

    # initialize any extensions and bind blueprints to the 
    # applicationn instance here   

    # flask_sqlalchemy initialization 
    db.init_app(app)

    # flask_migrate for migrations
    migrate.init_app(app)

    # flask_bcrypt to encrypt password
    flask_bcrypt.init_app(app)

    # flask_login to manage authentication
    login_manager.init_app(app)
    login_manager.login_view = "users.login"
    login_manager.login_message_category = 'info'

    from interviewapp.users.views import users
    app.register_blueprint(users, url_prefix='/users')

    from interviewapp.interviews.views import interviews
    app.register_blueprint(interviews, url_prefix='/')

    from interviewapp.questions.views import questions
    app.register_blueprint(questions, url_prefix='/questions')

    from interviewapp.topics.views import topics
    app.register_blueprint(topics, url_prefix='/topcis')

    from interviewapp.users import models as user_models
    @login_manager.user_loader
    def load_user(user_id):
        return user_models.User.query.get(int(user_id))

    return app 


