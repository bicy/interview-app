from flask import (
    Blueprint, 
    flash, redirect, 
    render_template, url_for,json, jsonify, request
    )

from flask_login import login_required
# Models 
from interviewapp.questions.models import Question
from interviewapp.topics.models import Topic

from interviewapp.questions.forms import AddQuestionForm
from interviewapp.users.views import login

# Blueprint
questions = Blueprint('questions', __name__, template_folder='questions')

@questions.route("/", methods=["GET", "POST"])
@login_required
def listing():
    page_title = "Question"
    questions = Question.query.all()
    return render_template(
        "questions/question_list.html", page_title=page_title,  questions=questions
    )
    
@questions.route("/add/", methods=["POST", "GET"])
def add():
    page_title = "new Question"
    questions = Question.query.all()
    form = AddQuestionForm()
    form.topic.choices = Topic.query.all()
    
    if request.method == 'POST':
        if form.validate_on_submit():
            form.save()
            flash("Question successfully added", "success")
            return redirect(url_for("questions.listing"))
        else:
            flash('Invalid data submited','danger')
            return redirect(url_for('questions.add'))
    
    return render_template(
        "questions/question_add.html", page_title=page_title, form=form, questions=questions
    )

# Changing Question according to Topic using ajax
@questions.route("/<topic_id>")
def get_question(topic_id):
    questions = Topic.query.filter_by(id=topic_id).first().questions

    question_data = []
    for q in questions:
        questionObj = {}
        questionObj["id"] = q.id
        questionObj["content"] = q.content
        question_data.append(questionObj)

    return jsonify({"questions": question_data})

@questions.route("/<id>/detail/", methods=["POST", "GET"])
@login_required
def question_detail(id):
    q = Question.query.filter_by(id=id).first()

    if request.is_json:
        json_object = json.dumps(q)
        return jsonify({"question": json_object})
    return render_template("questions/question_detail.html", question=q)
    



