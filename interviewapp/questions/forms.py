from flask_wtf import FlaskForm 
from wtforms import SubmitField, SelectField, TextAreaField, IntegerField
from wtforms.validators import DataRequired, Length, NumberRange

from interviewapp.topics.models import Topic
from interviewapp.questions.models import Question
from interviewapp import db 
from sqlalchemy.exc import SQLAlchemyError



class AddQuestionForm(FlaskForm):
    topic = SelectField(
        "Topic",
        choices=[],
        validate_choice=False,
        validators=[DataRequired()],
    )
    content = TextAreaField("Question", validators=[Length(max=500), DataRequired()])
    answer = TextAreaField("Answer", validators=[Length(max=500), DataRequired()])
    max_score = IntegerField(
        "Maximum Score",
        validators=[DataRequired(), NumberRange(min=1, max=10)],
    )
    submit = SubmitField("Submit")

    def save(self):
        content = self.content.data
        answer = self.answer.data
        score = self.max_score.data
        topic = self.topic.data
        t = Topic.query.filter_by(content=topic).first()
        q = Question(content=content, expected_answer=answer, max_score=score)
        t.questions.append(q)
        try:
            db.session.add(q)
            db.session.commit()
        except SQLAlchemyError:
            pass