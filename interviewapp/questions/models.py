from interviewapp import db 
from interviewapp.rates.models import Rate

class Question(db.Model):
    __tablename__ = "question"

    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text(500), unique=True)
    expected_answer = db.Column(db.Text(500))
    max_score = db.Column(db.Integer)
    rates = db.relationship("Rate", backref="question", lazy=True)

    def __repr__(self):
        return self.content