from flask_wtf import FlaskForm 
from wtforms import StringField, SubmitField, EmailField, RadioField, PasswordField, BooleanField
from wtforms.validators import DataRequired, InputRequired, Length
from sqlalchemy import exc
from flask import current_app
from interviewapp import db
from interviewapp.users.models import User
from password_generator import PasswordGenerator



class AddUserForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    email = EmailField("Email", validators=[DataRequired()])
    is_expert = BooleanField("expert")
    is_recruiter = BooleanField("recruiter")
    password = PasswordField()
    submit = SubmitField("Submit")


    def save(self):
        user = User(
            username=self.username.data, 
            email=self.email.data,
            password=self.password.data
        )

        if self.is_expert.data and self.is_recruiter.data:
            user.is_recruiter = True 
            user.is_specialist = True

        else:
            if self.is_recruiter.data:
                user.is_recruiter = True 

            if self.is_expert.data:
                user.is_specialist = True

        return user
       
            
           
class LoginForm(FlaskForm):
    email = EmailField("Email", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired(), Length(min=6)])
    submit = SubmitField("Sign in")
