from ast import match_case
from crypt import methods
from flask import (
    Blueprint, redirect, 
    request, url_for, 
    render_template, flash, render_template_string, current_app
    )

from flask_login import login_required, login_user, logout_user,current_user
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import exc
from interviewapp.users.forms import AddUserForm, LoginForm
from interviewapp.users.models import User
from interviewapp import flask_bcrypt, db
import smtplib
from decouple import config as environ
from email.message import EmailMessage

users = Blueprint('users', __name__)

@users.route("/")
@login_required
def listing():
    page_title = "users"
    users = User.query.all()
    count = len(users)
    return render_template('users/user_list.html',page_title=page_title,users=users,count=count)

@users.route('/add/', methods=['GET', 'POST'])
@login_required
def add():
    page_title = "new user"
    # new user form
    form = AddUserForm()
    # on POST request
    if request.method == 'POST':
        if form.validate_on_submit():
            # PasswordGenerator module to generate a password for each user
            from password_generator import PasswordGenerator
            pwo = PasswordGenerator()
            # set generated password max lenght characteres to be 8
            pwo.maxlen = 8
            # saving generated password
            password = pwo.generate()
            # setting generated password
            form.password.data = password
            # create user instance
            user = form.save()
            try:
                db.session.add(user)
                db.session.commit()
                 # sending email to the new user
           
                message = EmailMessage()
                # fetching email address from environment variable
                sender = environ('EMAIL_ADDRESS')
                recipient = form.email.data 
                username = form.username.data

                # handle mail sending proccess
                try:
                    message['Subject'] = 'Registration Interview.com'
                    message['From'] = sender 
                    message['To'] = form.email.data 
                    message.set_content('registered on interview')
                    message.add_alternative(
                    """
                    <p>Hello {0},</p>
                    <p>Here are your login and password on <a href='#' style='text-decoration:none'>Interview.com</a></p>
                    <p>Login: {1} <br> Password: {2}</p> 
                    """.format(username,recipient,password), subtype='html')
                    server = smtplib.SMTP('smtp.gmail.com',587)
                    server.starttls()
                    server.login(sender,environ('EMAIL_PASSWORD'))
                    server.send_message(message)
                    flash("user created and email sent to {0}".format(recipient), 'success')
                    return redirect(url_for('users.listing'))
                except smtplib.SMTPException as e:
                    # log the error
                    current_app.log_exception("Mail could not be sent to user")
                    flash('Error: unable to send email','warning')
                    return render_template("users/user_add.html", form=form,page_title=page_title)
            
            except exc.IntegrityError as e:
                # log the error here
                return render_template("users/user_add.html", form=form,page_title=page_title)
            except exc.SQLAlchemyError:
                # log the error here
                flash("Something wrnt wrong when creating this user !")
                return render_template("users/user_add.html", form=form,page_title=page_title)

        # when user send invalid data
        else:
            flash("Invalid data", 'warning')
    # on GET request
    return render_template(
        "users/user_add.html", 
        form=form,
        page_title=page_title
    )

@users.route("/search/", methods=['GET','POST'])
@login_required
def search():
    page_title = 'Search'
    user_list = User.query.all()
    # count present user into the database
    count = len(user_list)
    # user submit data on post method request
    if request.method == 'POST':
        users = User.query 
        query = request.form.get('search')
        # when user send empty string
        if not query:
            flash("warning: empty query was sent",'warning')
            # redirect user to interview list page
            return redirect(url_for('users.listing'))
        
        # user string is not empty
        page_title  = "{0} result".format(query)
        matching = users.filter(User.username.like("%" + query + "%")).all()
        
        # user query matches some data in the database
        if len(matching) > 0:
            # set user list to those matching the query
            user_list = matching 
            # count how many user match the query
            count = len(user_list)
        
        # matching length less than 0 nothing match user query
        else:
            flash('{0} does not match any user'.format(query),'info')
            return redirect(url_for('users.listing'))

    # GET request
    return render_template(
        'users/user_list.html',
         page_title=page_title,
         users=user_list,count=count
    )

@users.route('/filter/',methods=['GET','POST'])
def filter():
    page_title = 'users'
    users = User.query.all()
    count = len(users)
    matching = []
    if request.method == 'POST':
        role = request.form.get('role')
        if not role:
            return redirect(url_for('users.listing'))

        if role == 'expert':
            matching = User.query.filter_by(is_specialist=True).all()
            
        elif role == 'recruiter':
            matching = User.query.filter_by(is_recruiter=True).all()
            
        elif role == 'admin':
            matching = User.query.filter_by(is_admin=True).all()
        
        if len(matching) > 0:
            users = matching
            count = len(users)

    return render_template(
        'users/user_list.html',
        page_title=page_title,
        users=users,
        count=count
    )

        


@users.route('/delete/<id>/', methods=["GET", "POST"])
def delete(id):
    page_title ="Delete user"
    user_id = id
    user = None
    try:
        user = User.query.filter_by(id=user_id).one()
    except exc.NoResultFound:
        flash("User with {id} id does not exist".format(user_id),'danger')
        return redirect(url_for('users.delete'))

    if request.method == "POST":
        if not user == None:
            try:
                db.session.delete(user)
                db.session.commit()
                flash("{} deleted".format(user.email), 'info')
                return redirect(url_for('users.listing'))
            except exc.SQLAlchemyError:
                flash("Error: user could be deleted", 'warning')
    return render_template('users/user_delete.html',page_title=page_title,user=user)

@users.route('/login/', methods=['GET',"POST"])
def login():
    page_title = "Login"
    if current_user.is_authenticated:
        return redirect(url_for('interviews.home'))

    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()

            if not user:
                flash('No such user exsts','warning')
                return render_template('users/login.html',form=form, page_title=page_title)

            if(not flask_bcrypt.check_password_hash(user.password,form.password.data)):
                flash("Invalid password.", 'warning')
                return render_template('users/login.html', form=form, page_title=page_title)

            login_user(user)
            flash('Success!', 'success')
            return redirect(url_for('interviews.home'))
    return render_template('users/login.html', page_title = page_title, form=form)


@users.route('/logout/', methods=["GET"])
def logout():
    logout_user()
    return redirect(url_for('users.login'))
     