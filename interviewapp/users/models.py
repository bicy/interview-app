from interviewapp import db
from interviewapp import flask_bcrypt
from sqlalchemy.ext.hybrid import hybrid_property



class User(db.Model):
    __tablename__ = "user"
    __table_args__ = {"extend_existing": True}

    # The primary key for each record
    id = db.Column(db.Integer, primary_key=True)

    # The unique username for user
    username = db.Column(db.String(50), unique=True, nullable=False)

    # The unique emai for  user
    email = db.Column(db.String(100), unique=True, nullable=False)

    # The hashed password for the user
    _password = db.Column(db.String(60), unique=True, nullable=True)

    is_admin = db.Column(db.Boolean, default=False)
    
    is_recruiter = db.Column(db.Boolean, default=False)

    is_specialist = db.Column(db.Boolean, default=False)


    def __init__(self,username, email, password):
        self.username = username
        self.email = email
        self.password = password

    def __repr__(self):
        return "{0}".format(self.username)

    @hybrid_property
    def password(self):
        return self._password
    
    @password.setter
    def password(self,password):
        self._password = flask_bcrypt.generate_password_hash(password)

    def get_id(self):
        return self.id 

    def is_anonymous(self):
        return False 

    def is_authenticated(self):
        return True 

    def is_active(self):
        return True
        
    @classmethod
    def create_user(cls, username, email, password):
        user = User(username, email, password)
        return user

    @classmethod
    def create_super_user(cls,username, email, password):
        user = cls.create_user(username, email, password)
        user.is_admin = True 
        return user 

    